#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

int main()
{   int size;
	int deleteNum;
	int userChoice;
	cout << "Enter the size for dynamic array: ";
	cin >> size;
	
while (true)
	{
	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);
	
		for (int i = 0; i < size; i++)
		{
			int rng = rand() % 101;
			unordered.push(rng);
			ordered.push(rng);
		}

		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";



		cout << "\n\nWhat do you want to do:" << endl;
		cout << "1 - Remove element at index" << endl;
		cout << "2 - Search for element" << endl;
		cout << "3 - Expand and generate random values" << endl;

		cin >> userChoice;
		if (userChoice == 1)
		{
			cout << "Enter an index to remove: ";
		    cin >> deleteNum;


			for (int i = 0; i < size; i++)
			{
				if ((deleteNum) == i)
				{
					unordered.remove(deleteNum);
				}
			}



			for (int i = 0; i < size; i++)
			{
				if ((deleteNum) == i)
				{
					ordered.remove(deleteNum);
				}
			}

			for (int i = 0; i < unordered.getSize(); i++)
			{
				cout << unordered[i] << "   ";

			}
			cout << endl;
			for (int i = 0; i < ordered.getSize(); i++)
			{
				cout << ordered[i] << "   ";
			}
			cout << endl;
			system("pause");
			system("cls");
		}
		else if (userChoice == 2)
		{
			cout << "\n\nInput element to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			return 0;
			system("pause");
			system("cls");
		}

		else if (userChoice == 3)
		{
			cout << "\n\nInput size of expansion:";
			cin >> size;


			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			cout << "Arrays have been expanded:" << endl;
			for (int i = 0; i < unordered.getSize(); i++)
			{
				cout << unordered[i] << "   ";

			}
			cout << endl;
			for (int i = 0; i < ordered.getSize(); i++)
			{
				cout << ordered[i] << "   ";
			}
			system("pause");
			system("cls");
		}
	}
	system("pause");
}