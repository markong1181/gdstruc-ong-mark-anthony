#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "UnorderedArray.h"

using namespace std;

int main()
{

	srand(time(NULL));
	int userInput;
    int values;
	int deleteNum;
	int search;
	
	cout << "Size of the array: " << endl;
	cin >> userInput;

	UnorderedArray<int> group(userInput);
	for (int i = 0; i < userInput; i++)
	{
		
		values = rand() % 100 + 1;
		group.push(values);
	}

	for (int i = 0; i < group.getSize(); i++)
	{
		cout << i+1 << ". " << group[i] << endl;
	}


	cout << "Delete a number from any order " << endl;
	cout << "Your choice: "; cin >> deleteNum;


	for (int i = 0; i < userInput; i++)
	{
		if ( (deleteNum - 1) == i)
		{
			group.remove(deleteNum - 1);
		}
	}
		

	//for (int i = 0; i < userInput; i++)
	//	cout << userInput << endl;
	//
	for (int i = 0; i < group.getSize(); i++)
	{
		cout << i + 1 << ". " << group[i] << endl;
	}
	
	cout << "linear search: " << endl;
	cout << "Enter the number: ";
	cin >> search;
	for (int i = 0; i < userInput; i++)
	{
		if (search == group[i])
		{
			cout << "The index of the number is " << i << endl;
			return 0;
		}
		else if (search != group[i])
		{
            cout << "Number was not found" << endl;
	         return -1;
		}

	}
	

	system("pause");
	
}
