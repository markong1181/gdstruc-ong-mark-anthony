#include <iostream>
#include "stack.h"
#include "queue.h"

using namespace std;

int main()
{
	int arraySize;
	int userChoice;
	int pushNumber;


	cout << "Enter size of element sets: ";
	cin >> arraySize;
	stack<int> stack(arraySize);
	queue<int> queue(arraySize);
	while (true) {


		cout << "What do you want to do?" << endl;
		cout << "1 - push element" << endl;
		cout << "2 - pop element" << endl;
		cout << "3 - print everything then empty set" << endl;
		cin >> userChoice;

		if (userChoice == 1)
		{
			cout << "Enter a number: ";
			cin >> pushNumber;
			stack.push(pushNumber);
			queue.push(pushNumber);
			system("pause");
			system("cls");
		}

		else if (userChoice == 2)
		{
			cout << "You have popped the front elements." << endl;

			
			arraySize--;
			stack.pop();
			queue.pop();
			cout << "Stack:" << endl;
			for (int i = stack.getSize(); 0 < i  ; i--)
			{
				cout << stack[i] << endl;
			}
			cout << "Queue:" << endl;
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << endl;
			}

			system("pause");
			system("cls");
		}

		else if (userChoice == 3)
		{
			cout << "Stack:" << endl;
			for (int i = stack.getSize(); 0 < i ; i--)
			{
				cout << stack[i] << endl;
			}
			cout << "Queue:" << endl;
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << endl;
			}
			stack.remove(1);
			queue.remove(1);
			system("pause");
			system("cls");
			
			
		}

	}

	system("pause");
}